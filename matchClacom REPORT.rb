MIGRATION_FILE = Rails.root.join('match-clacom.csv')
DUMMY_CLACOM = Sodimac::Clacom.find_by(name: '123 - NO TIENE CLACOM', code: 'dummycode')
REPORT_CSV = []

ActiveRecord::Base.transaction do
  CSV.foreach(MIGRATION_FILE, headers: :first_row, col_sep: ';', quote_char: "\x00") do |row|
    category_code = row['ID GS1']
    clacom = row['Clacom']&.rjust(10, '0')
    name = row['Nombre clacom']

    puts "Init migration for Category Code: #{ category_code } - CLACOM: #{ name } ID: #{ clacom }"

    unless category_code.present?
      puts "ID GS1 is empty for CLACOM: #{ name } - ID: #{ clacom }"

      REPORT_CSV << [category_code, clacom, name, 'fail', 'ID GS1 is empty']
      next
    end

    category = Sodimac::Category.find_by(code: category_code)

    unless category
      puts "CATEGORY not found for CLACOM: #{ name } - ID: #{ clacom }"

      REPORT_CSV << [category_code, clacom, name, 'fail', 'CATEGORY not found']
      next
    end

    clacom = Sodimac::Clacom.find_by(code: clacom)
    
    if clacom
      clacom.name = name
      if clacom.save
        REPORT_CSV << [category_code, clacom, name, 'clacom_name_updated', clacom.previous_changes]
      end
    else
      clacom = DUMMY_CLACOM
    end

    category_clacom = Sodimac::CategoryClacom.find_by(
      category_id: category.id,
      sodimac_clacom_id: clacom.id
    )

    unless category_clacom
      new_category_clacom = Sodimac::CategoryClacom.create(
        category_id: category_clacom.id,
        sodimac_clacom_id: clacom_id
      )
      puts "CATEGORY CLACOM created for CATEGORY_ID: #{ category_clacom.id } - CLACOM_ID: #{ clacom_id }"

      REPORT_CSV << [category_code, clacom, name, 'category_clacom_created', new_category_clacom.to_json]
    else
      REPORT_CSV << [category_code, clacom, name, 'category_clacom_exist', category_clacom.to_json]
    end
  end

  file = 'match-clacom-result.csv'

  CSV.open( file, 'w' ) do |writer|
    writer << ['CATEGORY CODE', 'CLACOM', 'NAME', 'STATUS', 'DETAILS']
    REPORT_CSV.each{|item| writer << item }
  end
end



# s = Sodimac::CategoryClacom.where('created_at BETWEEN ? AND ?', Time.now - 4.years, Time.now )
# destruidas = []
# s.each do |destroyed|
#   destruidas << destroyed.id
#   destroyed.really_destroy!
# end
