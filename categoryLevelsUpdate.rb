
apk add git
git clone https://gitlab.com/gustavo-corp/gpc.git

#MIGRATION_FILE = Rails.root.join('gpc/tree-gpc.csv')
#MIGRATION_FILE = Rails.root.join('gcp/tree-gpc.csv')
MIGRATION_FILE = Rails.root.join('tree-gpc2.csv')
REPORT_CSV = []
NAMES = []
LEVELS = []
GPC_IDS = []
PIM_IDS = []

# Know File Headers, looks like %(Name	Level	ID_GPC	ID_PIM)
CSV.read(MIGRATION_FILE, headers: true,col_sep: ';', quote_char: "\x00").headers

CSV.foreach(MIGRATION_FILE, headers: :first_row, col_sep: ';', quote_char: "\x00") do |row|
  NAMES << row['Name']
  LEVELS << row['Level']
  GPC_IDS << row['ID_GPC']
  PIM_IDS << row['ID_PIM']
end
def get_level_category(level)
  level.gsub(/[^\d]/, '')&.to_i - 1
end
CATEGORIES = Array.new(4)
ActiveRecord::Base.transaction do
  current_index = 0
  NAMES.each do |name|
    #if current_index.next % 250 == 0
    #  puts "
    #
    #
    #   CURRENT UPDATE
    #    #{current_index}  #{}   #{}
    #
    #
    #    "
    #  sleep(30)
    #end
    current_id = get_level_category(LEVELS[current_index])
    parent = current_id.zero? ? nil : CATEGORIES[current_id - 1]

    name = NAMES[current_index]
    level = LEVELS[current_index]
    pim_id = PIM_IDS[current_index]
    gpc_id = GPC_IDS[current_index]
    if level == nil
      puts "        #{name}  #{}   #{}        "
    end
    puts "Init migration for NAME: #{name} | ID_GPC: #{gpc_id} | ID_PIM: #{pim_id}"

    CATEGORIES[current_index] = Sodimac::Category.find_or_initialize_by( code: gpc_id)
    CATEGORIES[current_index].update(
      name: name.parameterize(separator: '_'),
      display_name: name,
      external_reference_id: pim_id,
      parent: parent
    )
    status = CATEGORIES[current_index].persisted? ? 'updated' : 'created'

    if CATEGORIES[current_index].save # check if category is valid, change to .save to persist changes
      REPORT_CSV << [gpc_id, pim_id, level, parent&.id, status, CATEGORIES[current_index].previous_changes]
    else
      REPORT_CSV << [gpc_id, pim_id, level, parent&.id, status, CATEGORIES[current_index].errors.details.to_s]
    end

    current_index += 1
  end

  file = 'tree-gpc-results.csv'

  CSV.open( file, 'w' ) do |writer|
    writer << ['ID_GPC', 'ID_PIM', 'LEVEL', 'PARENT', 'STATUS', 'DETAILS']
    REPORT_CSV.each{|item| writer << item }
  end
end
